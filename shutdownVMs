#!/bin/bash

# This script will shutdown multiple VMs in a certain order
# based on the .autostart file located in /home/vkalra/bin
# You can also shutdown individual VMs using the UI name
# Or you can start a group of VMs, e.g. DB|IDM|IAM

# Declare what you need
declare -a vmUUID
declare -a vmName
declare -A vmAArr
declare -A vmGrp

# How long to wait between consecutive shutdowns
declare timeWait=30

usage()
{
  echo "Usage: shutdownVMs [ALL|DB|IDM|IAM|WEB|<Name of VM>]"
  exit 1;
}

# Check that at least one argument was supplied
[ -z "$1" ] && usage

# Process the .autostart file and place into hash
processVMString()
{
  IFS=$'\n'
  # Parse the autostart file and get the vm name and the uuid store in an array
  for line in $(grep -v "^#" /home/vkalra/bin/.autostart)
  do
    #echo "DEBUG: line is : $line"
    IFS=: vmArr=( $line )
    vmName+=(${vmArr[0]})
    vmUUID+=(${vmArr[1]})
    vmAArr+=([${vmArr[0]}]=${vmArr[1]})
    #echo "DEBUG: vm of 0 is |${vmArr[0]}| and vm of 1 is ${vmArr[1]} and vm of 2 is ${vmArr[2]}"
    if [ "${vmArr[2]}" == "DB" ]
    then
      vmGrp["DB"]+=${vmArr[1]}:
    fi
    if [ "${vmArr[2]}" == "IDM" ]
    then
      vmGrp["IDM"]+=${vmArr[1]}:
    fi
    if [ "${vmArr[2]}" == "IAM" ]
    then
      vmGrp["IAM"]+=${vmArr[1]}:
    fi
    if [ "${vmArr[2]}" == "WEB" ]
    then
      vmGrp["WEB"]+=${vmArr[1]}:
    fi
  done

     #echo "DEBUG: The array is:"
     #for i in ${vmGrp[@]}
     #do
     #echo "The value is $i"
     #done
  unset IFS
}
 # Is there an argument?
if [ $# -eq 1 ]
then
  processVMString
else
  usage
  exit
fi

case "$1" in
'ALL') echo "Shutting down ALL VMs in reverse order"
       for ((i=${#vmUUID[@]}-1; i>=0; i--))
       do
         echo "Shutting down ${vmUUID[$i]}"
         vboxmanage controlvm ${vmUUID[$i]} acpipowerbutton
         sleep $timeWait
       done
;;
'DB') 
      IFS=:
      # Check to see if there is a least one to shutdown
      if [ ${#vmGrp[DB]} -eq 0 ]
      then
        echo "Nothing to do, DB list is empty"
        exit
      fi

      echo "Shutting down VMs in DB Group"
      vm=${vmGrp[DB]}
      for v in ${vm}
      do
        echo "Shutting down $v"
        vboxmanage controlvm $v acpipowerbutton
        sleep $timeWait
      done
      unset IFS

;;
'IDM') 
       IFS=:
       # Check to see if there is a least one to shutdown
       if [ ${#vmGrp[IDM]} -eq 0 ]
       then
         echo "Nothing to do, IDM list is empty"
         exit
       fi

       echo "Shutting down VMs in IDM Group"
       vm=${vmGrp[IDM]}
       for v in ${vm}
       do
         echo "Shutting down $v"
         vboxmanage controlvm $v acpipowerbutton
         sleep $timeWait
       done
       unset IFS

;;
'IAM') 
       IFS=:
       # Check to see if there is a least one to shutdown
       if [ ${#vmGrp[IAM]} -eq 0 ]
       then
         echo "Nothing to do, IAM list is empty"
         exit
       fi

       echo "Shutting down VMs in IAM Group"
       vm=${vmGrp[IAM]}
       for v in ${vm}
       do
         echo "Shutting down $v"
         vboxmanage controlvm $v acpipowerbutton
         sleep $timeWait
       done
       unset IFS

;;
'WEB') 
       IFS=:
       # Check to see if there is a least one to shutdown
       if [ ${#vmGrp[WEB]} -eq 0 ]
       then
         echo "Nothing to do, WEB list is empty"
         exit
       fi

       echo "Shutting down VMs in WEB Group"
       vm=${vmGrp[WEB]}
       for v in ${vm}
       do
         echo "Shutting down $v"
         vboxmanage controlvm $v acpipowerbutton
         sleep $timeWait
       done
       unset IFS

;;
*) # This must be a named VM you are trying to shutdown
   echo Shutting down $1
   vboxmanage controlvm "$1" acpipowerbutton
;;
esac
